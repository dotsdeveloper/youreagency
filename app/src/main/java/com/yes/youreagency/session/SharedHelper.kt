package com.yes.youreagency.session

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yes.Responses.Responses.Order

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)

    var token: String
        get() : String {
            return sharedPreference.getKey("token")
        }
        set(value) {
            sharedPreference.putKey("token", value)
        }

    var refercode: String
        get() : String {
            return sharedPreference.getKey("refercode")
        }
        set(value) {
            sharedPreference.putKey("refercode", value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt("id")
        }
        set(value) {
            sharedPreference.putInt("id", value)
        }


    var fcmToken: String
        get() : String {
            return sharedPreference.getKey("fcmToken")
        }
        set(value) {
            sharedPreference.putKey("fcmToken", value)
        }



    var language: String
        get() : String {
            return if (sharedPreference.getKey("language") == "") {
                "en"
            } else {
                sharedPreference.getKey("language")
            }

        }
        set(value) {
            sharedPreference.putKey("language", value)
        }

    var branchid: Int
        get() : Int {
            return sharedPreference.getInt("branchid")
        }
        set(value) {
            sharedPreference.putInt("branchid", value)
        }

    var category_view: Int
        get() : Int {
            return sharedPreference.getInt("category_view")
        }
        set(value) {
            sharedPreference.putInt("category_view", value)
        }


    var name: String
        get() : String {
            return sharedPreference.getKey("name")
        }
        set(value) {
            sharedPreference.putKey("name", value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey("email")
        }
        set(value) {
            sharedPreference.putKey("email", value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey("mobileNumber")
        }
        set(value) {
            sharedPreference.putKey("mobileNumber", value)
        }

    var countryCode: String
        get() : String {
            return sharedPreference.getKey("countryCode")
        }
        set(value) {
            sharedPreference.putKey("countryCode", value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("loggedIn")
        }
        set(value) {
            sharedPreference.putBoolean("loggedIn", value)
        }

    var location: String
        get() : String {
            return sharedPreference.getKey("location")
        }
        set(value) {
            sharedPreference.putKey("location", value)
        }

    var primarycolor: String
        get() : String {
            return sharedPreference.getKey("primarycolor","#FF6200EE")
        }
        set(value) {
            sharedPreference.putKey("primarycolor", value)
        }

    var midbanner: String
        get() : String {
            return sharedPreference.getKey("midbanner")
        }
        set(value) {
            sharedPreference.putKey("midbanner", value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey("imageUploadPath")
        }
        set(value) {
            sharedPreference.putKey("imageUploadPath", value)
        }

    var lastnotify: Int
        get() : Int {
            return sharedPreference.getInt("lastnotify")
        }
        set(value) {
            sharedPreference.putInt("lastnotify", value)
        }

    var about: String
        get() : String {
            return sharedPreference.getKey("about")
        }
        set(value) {
            sharedPreference.putKey("about", value)
        }

    var contact_content: String
        get() : String {
            return sharedPreference.getKey("contact_content")
        }
        set(value) {
            sharedPreference.putKey("contact_content", value)
        }

    var help: String
        get() : String {
            return sharedPreference.getKey("help")
        }
        set(value) {
            sharedPreference.putKey("help", value)
        }

    var terms_condition: String
        get() : String {
            return sharedPreference.getKey("terms_condition")
        }
        set(value) {
            sharedPreference.putKey("terms_condition", value)
        }

    var privacy_policy: String
        get() : String {
            return sharedPreference.getKey("privacy_policy")
        }
        set(value) {
            sharedPreference.putKey("privacy_policy", value)
        }

    var refund_policy: String
        get() : String {
            return sharedPreference.getKey("refund_policy")
        }
        set(value) {
            sharedPreference.putKey("refund_policy", value)
        }

    var weburl: String
        get() : String {
            return sharedPreference.getKey("weburl")
        }
        set(value) {
            sharedPreference.putKey("weburl", value)
        }

    var shipping_policy: String
        get() : String {
            return sharedPreference.getKey("shipping_policy")
        }
        set(value) {
            sharedPreference.putKey("shipping_policy", value)
        }

    var popularhead: String
        get() : String {
            return sharedPreference.getKey("popularhead")
        }
        set(value) {
            sharedPreference.putKey("popularhead", value)
        }

    var productshead: String
        get() : String {
            return sharedPreference.getKey("productshead")
        }
        set(value) {
            sharedPreference.putKey("productshead", value)
        }

    var newarrivalhead: String
        get() : String {
            return sharedPreference.getKey("newarrivalhead")
        }
        set(value) {
            sharedPreference.putKey("newarrivalhead", value)
        }

    var bestsellinghead: String
        get() : String {
            return sharedPreference.getKey("bestsellinghead")
        }
        set(value) {
            sharedPreference.putKey("bestsellinghead", value)
        }

    var bestsellingcount: Int
        get() : Int {
            return sharedPreference.getInt("bestsellingcount")
        }
        set(value) {
            sharedPreference.putInt("bestsellingcount", value)
        }

    var newarrivalcount: Int
        get() : Int {
            return sharedPreference.getInt("newarrivalcount")
        }
        set(value) {
            sharedPreference.putInt("newarrivalcount", value)
        }

    var max_quantity: Int
        get() : Int {
            return sharedPreference.getInt("max_quantity")
        }
        set(value) {
            sharedPreference.putInt("max_quantity", value)
        }


    var stock_option: Int
        get() : Int {
            return sharedPreference.getInt("stock_option")
        }
        set(value) {
            sharedPreference.putInt("stock_option", value)
        }

    var whatsapp: String
        get() : String {
            return sharedPreference.getKey("whatsapp")
        }
        set(value) {
            sharedPreference.putKey("whatsapp", value)
        }

    var shopname: String
        get() : String {
            return sharedPreference.getKey("shopname")
        }
        set(value) {
            sharedPreference.putKey("shopname", value)
        }

    var notifycount: String
        get() : String {
            return sharedPreference.getKey("notifycount")
        }
        set(value) {
            sharedPreference.putKey("notifycount", value)
        }

    var walletamount: String
        get() : String {
            return sharedPreference.getKey("walletamount")
        }
        set(value) {
            sharedPreference.putKey("walletamount", value)
        }

    var referalamount: String
        get() : String {
            return sharedPreference.getKey("referalamount")
        }
        set(value) {
            sharedPreference.putKey("referalamount", value)
        }

    var shoppermobile: String
        get() : String {
            return sharedPreference.getKey("shoppermobile")
        }
        set(value) {
            sharedPreference.putKey("shoppermobile", value)
        }

    var shopperccode: String
        get() : String {
            return sharedPreference.getKey("shopperccode")
        }
        set(value) {
            sharedPreference.putKey("shopperccode", value)
        }

    var shoppermail: String
        get() : String {
            return sharedPreference.getKey("shoppermail")
        }
        set(value) {
            sharedPreference.putKey("shoppermail", value)
        }

    var payoption: String
        get() : String {
            return sharedPreference.getKey("payoption")
        }
        set(value) {
            sharedPreference.putKey("payoption", value)
        }

    var loader_common: String
        get() : String {
            return sharedPreference.getKey("loader_common")
        }
        set(value) {
            sharedPreference.putKey("loader_common", value)
        }

    var loader_cart: String
        get() : String {
            return sharedPreference.getKey("loader_cart")
        }
        set(value) {
            sharedPreference.putKey("loader_cart", value)
        }

    var currency: String
        get() : String {
            return sharedPreference.getKey("currency")
        }
        set(value) {
            sharedPreference.putKey("currency", value)
        }

    var share_content: String
        get() : String {
            return sharedPreference.getKey("share_content")
        }
        set(value) {
            sharedPreference.putKey("share_content", value)
        }

    var razorpayapi: String
        get() : String {
            return sharedPreference.getKey("razorpayapi")
        }
        set(value) {
            sharedPreference.putKey("razorpayapi", value)
        }

    var razorpayname: String
        get() : String {
            return sharedPreference.getKey("razorpayname")
        }
        set(value) {
            sharedPreference.putKey("razorpayname", value)
        }

    var cartid_temp: String
        get() : String {
            return sharedPreference.getKey("cartid_temp","")
        }
        set(value) {
            sharedPreference.putKey("cartid_temp", value)
        }

    var iscart_temp: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("iscart_temp")
        }
        set(value) {
            sharedPreference.putBoolean("iscart_temp", value)
        }

    var cartlist: ArrayList<Order>
        get() : ArrayList<Order> {
            val myType = object : TypeToken<List<Order>>() {}.type
            val vsl = sharedPreference.getKey("cartlist")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Order> = Gson().fromJson<List<Order>>(vsl, myType)
            return logs as ArrayList<Order>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cartlist", jsonString)
        }
}
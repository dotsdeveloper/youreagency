package com.yes.youreagency.network

import com.yes.youreagency.others.AppController
import com.yes.youreagency.R

object UrlHelper {
 private const val BASE = "https://yourestore.in/"

 private const val BASE_URL = BASE + "yes-backend/api/"
 const val MASTERID = "1"

 const val GETALLPRODUCT = BASE_URL + "customer/productbyshopper"

 const val ADDTOCART = BASE_URL
 const val GETCART = BASE_URL + "cart/"
 const val DELETECART = BASE_URL + "cart/"

 const val PLACEORDER = BASE_URL + "direct/store"
 const val GETORDERLIST = BASE_URL + "order_by_user/"

 const val CHECKCOUPON = BASE_URL + "cart/checkCouppen"
 const val GETTIMESLOT = BASE_URL + "get_time_slate/"
 const val GETADDRESS = BASE_URL + "direct/location"
 const val ADDADDRESS = BASE_URL + "add_location"

 const val LOGIN = BASE_URL + "login"
 const val GETCUSTOMERINFO = BASE_URL + "direct/get_customer_info"
 const val CARTMAPPING = BASE_URL + "direct/cart_mapping"
 const val REORDER = BASE_URL + "re_order/"
 const val GETINVOICE = BASE_URL + "invoice/"

 const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
 const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
 const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
 const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
 const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
 const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"


 fun getAddress(latitude: Double, longitude: Double): String? {
  val lat = latitude.toString()
  val lngg = longitude.toString()
  return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
          + lngg + "&sensor=true&key=" + AppController.getInstance().resources.getString(
   R.string.map_api_key
  ))
 }
}
package com.yes.youreagency.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.yes.Responses.Responses.GetCartResponse
import com.yes.youreagency.network.ApiInput
import com.yes.youreagency.network.UrlHelper
import com.yes.youreagency.repository.MapRepository
import com.yes.youreagency.responses.*
import com.yes.youreagency.session.Constants
import com.yes.youreagency.session.SharedHelper
import org.json.JSONArray
import org.json.JSONObject

class MapViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MapRepository = MapRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        if(sharedHelper!!.loggedIn){
            sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = "Bearer "+it.token }
        }
        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        Log.d("Headers:", "" + header)

        return apiInputs
    }

    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {
        return repository.getAddress(input)
    }

    fun login(context: Context, mobile: String, passsword: String): LiveData<LoginResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("shopper_id", sharedHelper?.branchid)
        jsonObject.put("mobile", mobile)
        jsonObject.put("password", passsword)
        return repository.login(getApiParams(context, jsonObject, UrlHelper.LOGIN))
    }

    fun addtocart(context: Context, qty: Int?, qtyid: Int?, productid: Int?): LiveData<AddCartResponse>? {
            var isnew = true
            val jsonObject1 = JSONObject()
            val jsonArray = JSONArray()

            if(sharedHelper!!.cartlist.size != 0){
                for(items in sharedHelper!!.cartlist){
                    if(items.product_id == productid) {
                        if(items.quantity_id == qtyid){
                            isnew = false
                            if(qty != 0){
                                val jsonObject = JSONObject()
                                jsonObject.put("shopper_id", sharedHelper!!.branchid)
                                jsonObject.put("product_id", productid)
                                jsonObject.put("quantity", qty)
                                jsonObject.put("quantity_id", qtyid)
                                jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
                                jsonArray.put(jsonObject)
                            }
                        }
                        else{
                            val jsonObject = JSONObject()
                            jsonObject.put("shopper_id", sharedHelper!!.branchid)
                            jsonObject.put("product_id", items.product_id)
                            jsonObject.put("quantity", items.quantity)
                            jsonObject.put("quantity_id", items.quantity_id)
                            jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
                            jsonArray.put(jsonObject)
                        }
                    }
                    else{
                        val jsonObject = JSONObject()
                        jsonObject.put("shopper_id", sharedHelper!!.branchid)
                        jsonObject.put("product_id", items.product_id)
                        jsonObject.put("quantity", items.quantity)
                        jsonObject.put("quantity_id", items.quantity_id)
                        jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
                        jsonArray.put(jsonObject)
                    }
                }
                if(isnew){
                    if(qty != 0) {
                        val jsonObject = JSONObject()
                        jsonObject.put("shopper_id", sharedHelper!!.branchid)
                        jsonObject.put("product_id", productid)
                        jsonObject.put("quantity", qty)
                        jsonObject.put("quantity_id", qtyid)
                        jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
                        jsonArray.put(jsonObject)
                    }
                }
            }
            else{
                if(qty != 0) {
                    val jsonObject = JSONObject()
                    jsonObject.put("shopper_id", sharedHelper!!.branchid)
                    jsonObject.put("product_id", productid)
                    jsonObject.put("quantity", qty)
                    jsonObject.put("quantity_id", qtyid)
                    jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
                    jsonArray.put(jsonObject)
                }
            }

            jsonObject1.put("data",jsonArray)

            if(jsonArray.length() == 0){
                val jsonObject = JSONObject()
                jsonObject.put("is_empty", 1)
                jsonObject.put("id", "")
                jsonObject.put("cartid", sharedHelper!!.cartid_temp)
                return repository.deletecart(getApiParams(context, jsonObject, UrlHelper.DELETECART+"customer_delete_woc"))
            }
            else{
                return repository.addtocart(getApiParams(context, jsonObject1, UrlHelper.ADDTOCART+"customer_cartAdd"))
            }
    }

    fun getcart(context: Context, cartid: String?): LiveData<GetCartResponse>? {
        return repository.getcart(getApiParams(context, null, UrlHelper.GETCART+sharedHelper!!.branchid+"/"+cartid))
    }

    fun deletecart(context: Context, cid: Int?): LiveData<AddCartResponse>? {
        if(sharedHelper!!.loggedIn){
            val jsonObject = JSONObject()
            if(cid == -1){
                jsonObject.put("is_empty", 1)
                jsonObject.put("id", "")
            }
            else{
                jsonObject.put("is_empty", 0)
                jsonObject.put("id", cid)
            }
            // jsonObject.put("cartid", sharedHelper!!.cartid_temp)

            return repository.deletecart(getApiParams(context, jsonObject, UrlHelper.DELETECART+"delete"))

        }
        else{
            val jsonObject = JSONObject()
            if(cid == -1){
                jsonObject.put("is_empty", 1)
                jsonObject.put("id", "")
            }
            else{
                jsonObject.put("is_empty", 0)
                jsonObject.put("id", cid)
            }
            jsonObject.put("cartid", sharedHelper!!.cartid_temp)

            return repository.deletecart(getApiParams(context, jsonObject, UrlHelper.DELETECART+"customer_delete_woc"))

        }
    }

    fun getallproduct(context: Context): LiveData<ProductlistResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("shopper_id", sharedHelper!!.branchid)
        return repository.getallproduct(getApiParams(context, jsonObject, UrlHelper.GETALLPRODUCT))
    }

    fun checkcoupon(context: Context,coupon:String,pin:String): LiveData<CheckCouponResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("shopper_id", sharedHelper!!.branchid)
        if(pin.isEmpty()){
            jsonObject.put("pincode", 0)
        }
        else{
            jsonObject.put("pincode", pin)
        }
        jsonObject.put("coupon", coupon)
        return repository.checkcoupon(getApiParams(context, jsonObject, UrlHelper.CHECKCOUPON))
    }

    fun getaddresslist(context: Context,mobile: String,name: String): LiveData<GetLocationResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("mobile",mobile )
        jsonObject.put("name", name)
        return repository.getaddresslist(getApiParams(context, jsonObject, UrlHelper.GETADDRESS))
    }

    fun addaddress(context: Context,name:String,address:String,landmark:String,lat:String,lng:String,pin:String): LiveData<CommonResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("location_name",name )
        jsonObject.put("address", address)
        jsonObject.put("landmark", landmark)
        jsonObject.put("location_lat", lat)
        jsonObject.put("location_long", lng)
        jsonObject.put("pincode", pin)
        return repository.addaddress(getApiParams(context, jsonObject, UrlHelper.ADDADDRESS))
    }

    fun gettimeslot(context: Context): LiveData<TimeslotResponse>? {
        return repository.gettimeslot(getApiParams(context, null, UrlHelper.GETTIMESLOT+sharedHelper!!.branchid))
    }

    fun getcustomerinfo(context: Context,mobile: String): LiveData<GetCustomerResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("mobile", mobile)
        return repository.getcustomerinfo(getApiParams(context, jsonObject, UrlHelper.GETCUSTOMERINFO))
    }

    fun cartmapping(context: Context,mobile: String,name : String): LiveData<CommonResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("cart_id", sharedHelper!!.cartid_temp)
        jsonObject.put("name", name)
        jsonObject.put("mobile", mobile)
        return repository.cartmapping(getApiParams(context, jsonObject, UrlHelper.CARTMAPPING))
    }

    fun getorders(context: Context): LiveData<GetOrdersResponse>? {
        return repository.getorders(getApiParams(context, null, UrlHelper.GETORDERLIST+sharedHelper!!.id))
    }

    fun getinvoice(context: Context,id: Int): LiveData<InvoiceResponse>? {
        return repository.getinvoice(getApiParams(context, null, UrlHelper.GETINVOICE+id))
    }

    fun reorder(context: Context,id:Int): LiveData<CommonResponse>? {
        return repository.reorder(getApiParams(context, null, UrlHelper.REORDER+""+id.toString()))
    }
}
package com.yes.youreagency.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.youreagency.activity.DashBoardActivity
import com.yes.youreagency.adapter.OrderListAdapter
import com.yes.youreagency.responses.OrderData
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.viewmodels.MapViewModel
import com.yes.youreagency.databinding.FragmentOrdersBinding


class OrdersFragment : Fragment() {
    var binding: FragmentOrdersBinding? = null
    var mapViewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null
    var orderlist: ArrayList<OrderData> = ArrayList<OrderData>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrdersBinding.inflate(inflater, container, false)
        val view = binding!!.root
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        UiUtils.linearlayout(binding!!.linear)
       // binding!!.top.text = sharedHelper!!.shopname
        binding!!.back.setOnClickListener {
            dashBoardActivity!!.removefragment(this)
        }
      /*  binding!!.search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search()
                return@OnEditorActionListener true
            }
            false
        })*/
        binding!!.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                search()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getorders(requireContext())?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                    }
                    else {
                        it.data!!.reverse()
                        orderlist = it.data!!
                        binding!!.recylerOrder.layoutManager = GridLayoutManager(requireContext(),1,
                            LinearLayoutManager.VERTICAL,false)
                        binding!!.recylerOrder.adapter = OrderListAdapter(
                            this,
                            requireContext(),
                            it.data)
                    }
                }
            }
        })
        binding!!.close.setOnClickListener {
            binding!!.search.setText("")
        }
        return view
    }

    fun search(){

        if(binding!!.search.text!!.isNotEmpty()){
            var searchorderlist: ArrayList<OrderData> = ArrayList<OrderData>()
            for(items in orderlist){
                if(items.invoice!!.toString().contains(binding!!.search.text.toString(),true)){
                    searchorderlist.add(items)
                   // Log.d("jhfjd123",""+searchorderlist.size)
                }
            }

            binding!!.recylerOrder.layoutManager = GridLayoutManager(requireContext(),1,
                LinearLayoutManager.VERTICAL,false)
            binding!!.recylerOrder.adapter = OrderListAdapter(
                this,
                requireContext(),
                searchorderlist)
        }
        else{
            binding!!.recylerOrder.layoutManager = GridLayoutManager(requireContext(),1,
                LinearLayoutManager.VERTICAL,false)
            binding!!.recylerOrder.adapter = OrderListAdapter(
                this,
                requireContext(),
                orderlist)
        }
    }

}
package com.yes.youreagency.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.Responses.Responses.GetCartData
import com.yes.Responses.Responses.Order
import com.yes.youreagency.activity.CustomerActivity
import com.yes.youreagency.activity.DashBoardActivity
import com.yes.youreagency.adapter.CartlistAdapter
import com.yes.youreagency.responses.Products
import com.yes.youreagency.responses.Quantities
import com.yes.youreagency.session.Constants
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.others.SwipeHelper
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.viewmodels.MapViewModel
import com.yes.youreagency.databinding.FragmentQuickOrderBinding
import java.util.ArrayList

class QuickOrderFragment : Fragment() {
    var binding: FragmentQuickOrderBinding? = null
    var mapViewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var dashBoardActivity: DashBoardActivity? = null
    var products: ArrayList<Products> = ArrayList<Products>()
    var quantities: ArrayList<Quantities> = ArrayList<Quantities>()
    var isfirst:Boolean = true
    lateinit var adapter:CartlistAdapter
    var productname: ArrayList<String> = ArrayList<String>()
    var quantity: ArrayList<String> = ArrayList<String>()
    var sqtystock:Int = 0

    var productid:Int = 0
    var qtyid:Int = 0
    var qty:Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentQuickOrderBinding.inflate(inflater, container, false)
        val view = binding!!.root
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)

        UiUtils.relativelayout(binding!!.header.linear)

        UiUtils.button(binding!!.add)
        UiUtils.button(binding!!.proceed)

        binding!!.header.name.visibility = View.VISIBLE
        binding!!.header.name.text = "Quick Order"
        binding!!.header.exit.setOnClickListener {
            dashBoardActivity!!.logout()
        }

        binding!!.add.setOnClickListener {
            if(productid != 0 && qtyid != 0 && qty != 0){
                if(iscart() == false){
                    addcart()
                }
                else{
                    UiUtils.showSnack(binding!!.root,"Product Already in Cart")
                }
            }
            else{
                UiUtils.showSnack(binding!!.root,"Please Choose Details")
            }
        }

        binding!!.proceed.setOnClickListener {
            startActivity(Intent(requireContext(), CustomerActivity::class.java))
        }

        binding!!.qty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if(binding!!.qty.text!!.isNotEmpty() && qtyid!=0){
                    qty = binding!!.qty.text!!.toString().toInt()
                    if(quantities.size != 0){
                        for(items in products){
                            if(items.id == productid){
                                for(items1 in items.quantities!!){
                                    if(items1.id == qtyid){
                                        binding!!.total.setText(""+ Constants.Common.CURRENCY+""+(items1.amount!!.toDouble()*qty))
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    qty = 0
                    binding!!.total.setText("")
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        getcart(true,false,false,false)
        getallproducts()
        return view
    }

    fun getcart(isget:Boolean,isadd:Boolean,ismodify:Boolean,isdelete:Boolean){
        // DialogUtils.showCartLoader(requireContext())
        if(!sharedHelper!!.cartid_temp.equals("")){
            mapViewModel?.getcart(requireContext(),sharedHelper!!.cartid_temp)?.observe(viewLifecycleOwner, Observer {
                // DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            //UiUtils.showSnack(binding!!.root, it.message!!)
                            sharedHelper!!.cartlist = ArrayList()
                            binding!!.proceed.visibility = View.GONE
                            binding!!.totalitemTxt.visibility = View.GONE
                        }
                        else {
                            if(it.getcart_data!!.order!!.size > 0){
                                binding!!.proceed.visibility = View.VISIBLE
                                binding!!.totalitemTxt.visibility = View.VISIBLE
                                binding!!.totalitemTxt.text = "Total Items : "+it.getcart_data!!.order!!.size

                                for((index, value) in it.getcart_data!!.order!!.withIndex()){
                                    value.product!!.category!!.ccount = getcatcount(it.getcart_data!!,it.getcart_data!!.order!![index].product!!.category!!.name.toString())
                                    //  it.getcart_data!!.order!![index].product!!.category!!.ccount = getcatcount(it.getcart_data!!,it.getcart_data!!.order!![index].product!!.category!!.name.toString())
                                }

                                it.getcart_data!!.order!!.sortBy { order: Order -> order.product!!.category!!.name }
                                sharedHelper!!.cartlist = it.getcart_data!!.order!!
                                if(isget || isadd) {
                                    binding!!.cartlist.layoutManager = GridLayoutManager(
                                        requireContext(),
                                        1,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
                                    adapter = CartlistAdapter(
                                        this,
                                        1,
                                        requireContext(),
                                        sharedHelper!!.cartlist
                                    )
                                    binding!!.cartlist.adapter = adapter
                                }
                                else if(ismodify || isdelete){

                                }

                                if(isfirst) {
                                    isfirst = false
                                    val swipeHelper: SwipeHelper = object : SwipeHelper(requireContext(), binding!!.cartlist) {
                                            override fun instantiateUnderlayButton(
                                                viewHolder: RecyclerView.ViewHolder,
                                                underlayButtons: ArrayList<UnderlayButton>
                                            ) {
                                                underlayButtons.add(UnderlayButton(
                                                    "Delete",
                                                    0,
                                                    Color.parseColor(sharedHelper!!.primarycolor)
                                                ) { pos: Int ->
                                                    DialogUtils.showLoader(requireContext())
                                                    mapViewModel?.deletecart(
                                                        requireContext(),
                                                        sharedHelper!!.cartlist[pos].id!!.toInt()
                                                    )
                                                        ?.observe(viewLifecycleOwner, Observer {
                                                            DialogUtils.dismissLoader()
                                                            it?.let {
                                                                it.error?.let { error ->
                                                                    if (error) {
                                                                        UiUtils.showSnack(
                                                                            binding!!.root,
                                                                            it.message!!
                                                                        )
                                                                    } else {
                                                                        var scatname = adapter.list!![pos].product!!.category!!.name.toString()
                                                                        var scatcount = adapter.list!![pos].product!!.category!!.ccount!!.toInt()
                                                                        for(items in adapter.list!!){
                                                                            if(items.product!!.category!!.name.equals(scatname)){
                                                                                items.product!!.category!!.ccount = scatcount-1
                                                                            }
                                                                        }
                                                                        adapter.list!!.removeAt(pos)
                                                                        adapter.notifyItemRemoved(pos)
                                                                        adapter.notifyDataSetChanged()
                                                                        getcart(false,false,false,true)
                                                                    }
                                                                }
                                                            }
                                                        })
                                                })
                                            }
                                        }
                                    swipeHelper.attachSwipe()
                                }


                            }
                            else{
                                sharedHelper!!.cartlist = ArrayList()
                                binding!!.proceed.visibility = View.GONE
                                binding!!.totalitemTxt.visibility = View.GONE
                            }

                            productspinner()
                        }
                    }
                }
            })
        }
        else{
            sharedHelper!!.cartlist = ArrayList()
            binding!!.proceed.visibility = View.GONE
            binding!!.totalitemTxt.visibility = View.GONE
        }
    }

    fun getcatcount(getcartData: GetCartData, c: String): Int {
        var count = 0
        for(items in getcartData.order!!){
            if(items.product!!.category!!.name.toString().equals(c)){
                count++
            }
        }
        return count
    }

    fun getallproducts(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getallproduct(requireContext())?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.message!!)
                        productname = ArrayList()
                        quantity = ArrayList()
                        productname.add("Select Product")
                        quantity.add("Select Quantity")
                        productspinner()
                    }
                    else {
                        it.product_data?.let { data ->
                            if (data.size != 0) {
                                products = data
                                productname = ArrayList()
                                quantity = ArrayList()
                                productname.add("Select Product")
                                quantity.add("Select Quantity")
                                for(items in data){
                                    productname.add(items.name!!)
                                }
                                productspinner()
                            }
                        }
                    }
                }
            }
        })
    }

    fun productspinner(){
        val searchmethod = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,productname)
        searchmethod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerProduct.adapter = searchmethod

        // spinner on item selected listener
        binding!!.spinnerProduct.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // textView.text = "Selected: "
                // get selected item text
                //textView.append(parent.getItemAtPosition(position).toString())

                Log.d("dhvgdhfvh","hcbjx"+parent.getItemAtPosition(position).toString())
                Log.d("dhvgdhfvh","hcbjx"+position)

                if(position != 0){
                    quantities = ArrayList<Quantities>()
                    quantity = ArrayList<String>()
                    for((index, value) in products.withIndex()){
                        if((position-1) == index){
                            productid = value.id!!
                            quantities = value.quantities!!
                            quantity.add("Select Quantity")
                            for(items in quantities){
                                quantity.add(items.quantity!!+" "+items.product_unit!!.name)
                            }
                            qtyspinner()
                        }
                    }
                }
                else{
                    productid = 0
                    qtyid = 0
                    binding!!.total.setText("")
                    quantity = ArrayList<String>()
                    quantity.add("Select Quantity")
                    qtyspinner()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }
    }

    fun qtyspinner(){
        val searchmethod = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,quantity)
        searchmethod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerQty.adapter = searchmethod

        // spinner on item selected listener
        binding!!.spinnerQty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // textView.text = "Selected: "
                // get selected item text
                //textView.append(parent.getItemAtPosition(position).toString())

                Log.d("dhvgdhfvh","hcbjx"+parent.getItemAtPosition(position).toString())
                Log.d("dhvgdhfvh","hcbjx"+position)
                if(position != 0){
                    qtyid = quantities[position-1].id!!
                    sqtystock = quantities[position-1].stock!!.toInt()
                    binding!!.qty.isEnabled = true
                }
                else{
                    qtyid = 0
                    binding!!.total.setText("")
                    quantity = ArrayList<String>()
                    binding!!.qty.isEnabled = false
                    binding!!.qty.setText("")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }

    fun addcart(){
        if(sharedHelper!!.stock_option == 1){
            if(qty > sharedHelper!!.max_quantity && sharedHelper!!.max_quantity != 0) {
                UiUtils.showSnack(binding!!.root,"Sorry you can not add more quantity.")
            }
            else{
                if((sharedHelper!!.max_quantity == 0 || sharedHelper!!.max_quantity > qty) && qty < sqtystock){
                    DialogUtils.showLoader(requireContext())
                    mapViewModel?.addtocart(requireContext(),qty,qtyid,productid)
                        ?.observe(viewLifecycleOwner, Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding!!.root, it.message!!)
                                    } else {
                                        sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                        getcart(false,true,false,false)
                                    }
                                }
                            }
                        })
                }
                else{
                    UiUtils.showSnack(binding!!.root,"Out of Stock")
                }
            }
        }
        else if(sharedHelper!!.stock_option == 0){
            if(qty > sharedHelper!!.max_quantity && sharedHelper!!.max_quantity != 0){
                UiUtils.showSnack(binding!!.root,"Sorry you can not add more quantity.")
            }
            else{
                DialogUtils.showLoader(requireContext())
                mapViewModel?.addtocart(requireContext(),qty,qtyid,productid)
                    ?.observe(viewLifecycleOwner, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding!!.root, it.message!!)
                                } else {
                                    sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                    getcart(false,true,false,false)
                                }
                            }
                        }
                    })
            }
        }
    }

    fun iscart() : Boolean {
        if(sharedHelper!!.cartlist.size != 0){
            for(items in sharedHelper!!.cartlist){
                if(items.product_id == productid){
                    if(items.quantity_id == qtyid){
                        return true
                    }
                }
            }
        }
        else{
            return false
        }
        return false
    }

}
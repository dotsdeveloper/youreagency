package com.yes.youreagency.fragment

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.Responses.Responses.Order
import com.yes.youreagency.R
import com.yes.youreagency.activity.DashBoardActivity
import com.yes.youreagency.adapter.ProductDetailsAdapter
import com.yes.youreagency.databinding.FragmentOrderDetailBinding
import com.yes.youreagency.responses.OrderData
import com.yes.youreagency.session.Constants
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.utils.BaseUtils
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.viewmodels.MapViewModel


class OrderDetailFragment : Fragment() {
    var binding: FragmentOrderDetailBinding? = null
    var mapViewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var orderdata = OrderData()
    var dashBoardActivity: DashBoardActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrderDetailBinding.inflate(inflater, container, false)
        val view = binding!!.root
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity?)
        UiUtils.relativelayout(binding!!.linear)
        UiUtils.button(binding!!.reorder)
        UiUtils.textview(binding!!.optxt)

        binding!!.back.setOnClickListener {
            dashBoardActivity!!.removefragment(this)
        }
        orderdata = arguments!!.getSerializable("key") as OrderData

        binding!!.orderid.text = orderdata.invoice

        binding!!.shop.text = orderdata.shopper!!.name.toString()
        binding!!.shopmobile.text = orderdata.shopper!!.mobile.toString()

        if(orderdata.delivered_at != null) {
            binding!!.deliveryOn.text = "Delivery Date & Time : "+ BaseUtils.getFormatedDate(orderdata.delivered_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","MMMM dd yyyy hh:mm aaa")
        }
        else{
            binding!!.deliveryOn.visibility = View.GONE
        }

        binding!!.placedOn.text = "Placed on : "+BaseUtils.getFormatedDate(orderdata.created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","MMMM dd yyyy hh:mm aaa")


        for((index, value) in orderdata.items!!.withIndex()){
            value.product!!.category!!.ccount = getcatcount(orderdata,orderdata.items!![index].product!!.category!!.name.toString())
           // orderdata.items!![index].product!!.category!!.ccount = getcatcount(orderdata,orderdata.items!![index].product!!.category!!.name.toString())
        }

        orderdata.items!!.sortBy { order: Order -> order.product!!.category!!.name }

        binding!!.recyclerProducts.layoutManager = GridLayoutManager(requireContext(),1,
            LinearLayoutManager.VERTICAL,false)
        binding!!.recyclerProducts.adapter = ProductDetailsAdapter(
            this,
            requireContext(),
            orderdata.items)

        binding!!.subtotal.text = Constants.Common.CURRENCY+orderdata.total
        binding!!.gstAmount.text = Constants.Common.CURRENCY+orderdata.gst_value
        binding!!.gstPer.text = "GST ("+orderdata.gst+"%)"
        binding!!.total2.text = Constants.Common.CURRENCY+orderdata.grand_total
        binding!!.total3.text = Constants.Common.CURRENCY+orderdata.paid_amount

        track(orderdata.status.toString().toInt())

        if(orderdata.file != null){
            binding!!.file.visibility = View.VISIBLE
            UiUtils.loadImage(binding!!.file, orderdata.file)
        }
        else{
            binding!!.file.visibility = View.GONE
        }


        if(orderdata.charge_amount!!.toString().toDouble() != 0.0){
            binding!!.linearDeliverycharge.visibility = View.VISIBLE
            binding!!.deliveryAmount.text = Constants.Common.CURRENCY+orderdata.charge_amount
        }

        if(orderdata.order_type.toString().toInt() == 1){
            binding!!.payMode.text = "Payment Mode : UPI"
        }
        else if(orderdata.order_type.toString().toInt() == 2){
            binding!!.payMode.text = "Payment Mode : Cash on delivery"

        }
        else if(orderdata.order_type.toString().toInt() == 3){
            binding!!.payMode.text = "Payment Mode : Card"

        }
        else if(orderdata.order_type.toString().toInt() == 4){
            binding!!.payMode.text = "Payment Mode : Wallet"

        }
        else if(orderdata.order_type.toString().toInt() == 5){
            binding!!.payMode.text = "Payment Mode : Wallet+"

        }


        binding!!.share.setOnClickListener {
            DialogUtils.showLoader(requireContext())
            mapViewModel?.getinvoice(requireContext(),orderdata.id!!)?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        }
                        else {
                            val shareIntent = Intent()
                            shareIntent.action = Intent.ACTION_SEND
                            shareIntent.type="text/plain"
                            var string = it.data
                            shareIntent.putExtra(Intent.EXTRA_TEXT, string);
                            startActivity(Intent.createChooser(shareIntent,"Share With"))
                        }
                    }
                }
            })
        }
        binding!!.reorder.setOnClickListener {
            DialogUtils.showLoader(requireContext())
            mapViewModel?.reorder(requireContext(),orderdata.id!!)?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding!!.root, it.message!!)
                        }
                        else {
                            dashBoardActivity!!.addfragment(QuickOrderFragment(),null)
                        }
                    }
                }
            })
        }
        return view
    }


    fun getcatcount(orderdata: OrderData, c: String): Int {
        var count = 0
        for(items in orderdata.items!!){
            if(items.product!!.category!!.name.toString().equals(c)){
                count++
            }
        }
        return count
    }

    fun track(status: Int){
        if(status == 1){
            binding!!.txtOrderplaced.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderplaced.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderaccepted.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line1.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderaccepted.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))

            binding!!.txtOrderassigned.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line2.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderassigned.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))

            binding!!.txtOrderdelivered.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line3.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderdelivered.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
        }
        else if(status == 2){

        }
        else if(status == 3){

        }
        else if(status == 4){
            binding!!.txtOrderplaced.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderplaced.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderaccepted.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line1.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderaccepted.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderassigned.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line2.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderassigned.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))

            binding!!.txtOrderdelivered.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line3.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderdelivered.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))

        }
        else if(status == 5){
            binding!!.txtOrderplaced.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderplaced.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderaccepted.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line1.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderaccepted.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderassigned.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line2.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderassigned.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderdelivered.setTextColor(getResources().getColor(R.color.black,null))
            binding!!.line3.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))
            binding!!.viewOrderdelivered.backgroundTintList = ColorStateList.valueOf(getResources().getColor(R.color.grey,null))

        }
        else if(status == 6){
            binding!!.txtOrderplaced.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderplaced.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderaccepted.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line1.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderaccepted.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderassigned.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line2.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderassigned.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

            binding!!.txtOrderdelivered.setTextColor(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.line3.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))
            binding!!.viewOrderdelivered.backgroundTintList = ColorStateList.valueOf(Color.parseColor(sharedHelper!!.primarycolor))

        }
    }

}
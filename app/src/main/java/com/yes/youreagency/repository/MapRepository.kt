package com.yes.youreagency.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yes.youreagency.network.Api
import com.yes.youreagency.network.ApiInput
import com.google.gson.Gson
import com.yes.Responses.Responses.GetCartResponse
import com.yes.youreagency.`interface`.ApiResponseCallback
import com.yes.youreagency.responses.*
import com.yes.youreagency.session.Constants
import com.yes.youreagency.utils.BaseUtils
import org.json.JSONArray
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository = MapRepository()
            }
            return repository as MapRepository
        }
    }




    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {

        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AutoCompleteResponse =
                    gson.fromJson(jsonObject.toString(), AutoCompleteResponse::class.java)
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AutoCompleteResponse()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun login(input: ApiInput): LiveData<LoginResponse>? {
        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: LoginResponse =
                    gson.fromJson(jsonObject.toString(), LoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = LoginResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun addtocart(input: ApiInput): LiveData<AddCartResponse>? {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getcart(input: ApiInput): LiveData<GetCartResponse>? {
        val apiResponse: MutableLiveData<GetCartResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetCartResponse =
                    gson.fromJson(jsonObject.toString(), GetCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun deletecart(input: ApiInput): LiveData<AddCartResponse>? {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getallproduct(input: ApiInput): LiveData<ProductlistResponse>? {
        val apiResponse: MutableLiveData<ProductlistResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean("error")){
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put("data",productScan(jsonObject.getJSONArray("data")))
                    val response: ProductlistResponse =
                        gson.fromJson(jsonObject.toString(), ProductlistResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                var response = ProductlistResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun productScan(jsonArray: JSONArray): JSONArray {
        for (i in 0 until jsonArray.length()) {
            if(jsonArray.getJSONObject(i).has("files")){
                val files = jsonArray.getJSONObject(i).get("files")
                if(!BaseUtils.isArray(files)){
                    jsonArray.getJSONObject(i).put("files", JSONArray())
                }
            }
            if(jsonArray.getJSONObject(i).has("original_files")){
                val oFiles = jsonArray.getJSONObject(i).get("original_files")
                if(!BaseUtils.isArray(oFiles)){
                    jsonArray.getJSONObject(i).put("original_files", JSONArray())
                }
            }
        }
        return jsonArray
    }
    fun checkcoupon(input: ApiInput): LiveData<CheckCouponResponse>? {
        val apiResponse: MutableLiveData<CheckCouponResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CheckCouponResponse =
                    gson.fromJson(jsonObject.toString(), CheckCouponResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CheckCouponResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getaddresslist(input: ApiInput): LiveData<GetLocationResponse>? {
        val apiResponse: MutableLiveData<GetLocationResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetLocationResponse =
                    gson.fromJson(jsonObject.toString(), GetLocationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetLocationResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun addaddress(input: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun gettimeslot(input: ApiInput): LiveData<TimeslotResponse>? {
        val apiResponse: MutableLiveData<TimeslotResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TimeslotResponse =
                    gson.fromJson(jsonObject.toString(), TimeslotResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TimeslotResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getcustomerinfo(input: ApiInput): LiveData<GetCustomerResponse>? {
        val apiResponse: MutableLiveData<GetCustomerResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetCustomerResponse =
                    gson.fromJson(jsonObject.toString(), GetCustomerResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetCustomerResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun cartmapping(input: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getorders(input: ApiInput): LiveData<GetOrdersResponse>? {
        val apiResponse: MutableLiveData<GetOrdersResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean("error")){
                    val response: GetOrdersResponse =
                        gson.fromJson(jsonObject.toString(), GetOrdersResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    val data = jsonObject.getJSONArray("data")
                    for(i in 0 until data.length()){
                        val items =  data.getJSONObject(i).getJSONArray("items")
                        for(j in 0 until items.length()){
                            val product = items.getJSONObject(j).getJSONObject("product")
                            items.getJSONObject(j).put("product",productScan(JSONArray().put(0,product))[0])
                        }
                    }
                    val response: GetOrdersResponse =
                        gson.fromJson(jsonObject.toString(), GetOrdersResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                var response = GetOrdersResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun reorder(input: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })
        return apiResponse
    }

    fun getinvoice(input: ApiInput): LiveData<InvoiceResponse>? {
        val apiResponse: MutableLiveData<InvoiceResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: InvoiceResponse =
                    gson.fromJson(jsonObject.toString(), InvoiceResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = InvoiceResponse()
                response.error = true
                apiResponse.value = response
            }
        })
        return apiResponse
    }

}
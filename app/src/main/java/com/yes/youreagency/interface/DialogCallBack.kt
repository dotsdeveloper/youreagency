package com.yes.youreagency.`interface`

interface DialogCallBack {
    fun onPositiveClick(value : String)
    fun onNegativeClick()
}
package com.yes.youreagency.`interface`

interface AnimationCallBack {
    fun onAnimationStart()
    fun onAnimationEnd()
    fun onAnimationRepeat()
}
package com.yes.youreagency.utils

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import android.text.style.StrikethroughSpan

import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics

import android.content.Context
import android.content.res.ColorStateList

import android.graphics.drawable.Drawable

import android.net.ParseException
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import java.text.SimpleDateFormat
import java.util.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.cardview.widget.CardView
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.multidex.BuildConfig
import com.google.android.material.button.MaterialButton
import com.yes.youreagency.`interface`.AnimationCallBack
import com.yes.youreagency.R
import com.yes.youreagency.session.SharedHelper

object UiUtils {
    fun loadImage(imageView: ImageView?, imageUrl: String?) {
        if (imageUrl == null || imageView == null || !imageUrl.contains("http")) {
            return
        }

        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.banner_loader1)
                    .error(R.mipmap.ic_launcher)
            )
            .into(imageView)

    }

    fun loadImage(imageView: ImageView?, imageUrl: String?, placeHolder: Drawable) {
        if (imageUrl == null || imageView == null || !imageUrl.contains("http")) {
            return
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER)
        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(placeHolder)
                    .error(R.drawable.no_image)
            )
            .into(imageView)

    }


    fun animation(context: Context,view: View,type: Int,visible: Boolean){
        var anim: Animation = AnimationUtils.loadAnimation(context,type)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                if(visible){
                    view.alpha = 1F
                    view.visibility = View.VISIBLE
                }
                else{
                    view.alpha = 1F
                    view.visibility = View.GONE
                }
            }
            override fun onAnimationRepeat(p0: Animation?) {

            }
        })
        view.startAnimation(anim)
    }

    fun animationwithcalback(context: Context,view: View,type: Int, callBack: AnimationCallBack){
        var anim: Animation = AnimationUtils.loadAnimation(context,type)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
                callBack.onAnimationStart()
            }

            override fun onAnimationEnd(p0: Animation?) {
               callBack.onAnimationEnd()
            }
            override fun onAnimationRepeat(p0: Animation?) {
                callBack.onAnimationRepeat()
            }
        })
        view.startAnimation(anim)
    }

    fun showSnack(view: View, content: String) {
        Snackbar.make(view, content, Snackbar.LENGTH_SHORT).show()
    }

    fun showLog(TAG: String, content: String) {
        Log.d(TAG, content)
    }

    fun convert(text : String): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
           var res = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
            return res.toString()
        }
        else {
            var res = Html.fromHtml(text)
            return res.toString()
        }
    }

    fun convertstringstrike(text : String): String {
        var string: Spannable = SpannableString(text)
        string.setSpan(StrikethroughSpan(), 0, string.length, 0)

        Log.d("hfghd3",""+string)
        Log.d("hfghd4",""+string.toString())

        string.setSpan(ForegroundColorSpan(Color.RED), 0, string.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        Log.d("hfghd1",""+string)
        Log.d("hfghd2",""+string.toString())

        return string.toString()
    }

    fun textview(textView: TextView){
        textView.setTextColor(Color.parseColor(SharedHelper(textView.context).primarycolor))
    }

    fun textviewbg(textView: TextView){
        textView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(SharedHelper(textView.context).primarycolor), BlendModeCompat.SRC_ATOP)
     /* val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.setColor(Color.parseColor(SharedHelper(textView.context).primarycolor))
        shape.cornerRadius = 8F
        textView.background = shape*/
    }

    fun radiobuttontint(radioButton: RadioButton){
        radioButton.buttonTintList = ColorStateList.valueOf(Color.parseColor(SharedHelper(radioButton.context).primarycolor))
    }

    fun checkboxtint(checkBox: CheckBox){
        checkBox.buttonTintList = ColorStateList.valueOf(Color.parseColor(SharedHelper(checkBox.context).primarycolor))
    }

    fun imageviewtint(imageView: ImageView){
        imageView.imageTintList = ColorStateList.valueOf(Color.parseColor(SharedHelper(imageView.context).primarycolor))
    }

    fun cardview(cardView: CardView){
        cardView.setCardBackgroundColor(Color.parseColor(SharedHelper(cardView.context).primarycolor))
    }

    fun cardviewbg(cardView: CardView){
        cardView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(SharedHelper(cardView.context).primarycolor), BlendModeCompat.SRC_ATOP)
    }

    fun linearlayout(linearLayout: LinearLayout){
        linearLayout.setBackgroundColor(Color.parseColor(SharedHelper(linearLayout.context).primarycolor))
    }

    fun relativelayout(relativeLayout: RelativeLayout){
        relativeLayout.setBackgroundColor(Color.parseColor(SharedHelper(relativeLayout.context).primarycolor))
    }

    fun button(button: Button){
        button.setBackgroundColor(Color.parseColor(SharedHelper(button.context).primarycolor))
    }

    fun notificationbar(activity: Activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = activity.getWindow()
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.setStatusBarColor(Color.parseColor(SharedHelper(activity).primarycolor))
        }
    }

   /* fun plus_minus(imageView: ImageView){
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.setColor(Color.WHITE)
        shape.setStroke(1, Color.parseColor(SharedHelper(imageView.context).primarycolor))
        shape.cornerRadius = 5F
        imageView.background = shape
        imageView.imageTintList = ColorStateList.valueOf(Color.parseColor(SharedHelper(imageView.context).primarycolor))
    }*/

    fun plus_minus(button: MaterialButton){
        button.iconTint = ColorStateList.valueOf(Color.parseColor(SharedHelper(button.context).primarycolor))
        button.strokeColor = ColorStateList.valueOf(Color.parseColor(SharedHelper(button.context).primarycolor))
    }

    fun offerborder(textView: TextView){
        textView.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.parseColor(SharedHelper(textView.context).primarycolor), BlendModeCompat.SRC_ATOP)

    /*    val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.setColor(Color.parseColor(SharedHelper(textView.context).primarycolor))
       // shape.cornerRadii = floatArrayOf(0f, 0f, 0f, 10f)
        textView.background = shape
        //textView.backgroundTintList = ColorStateList.valueOf(Color.parseColor(SharedHelper(textView.context).primarycolor))
*/    }

    fun navigateToFragment(
        manager: FragmentManager,
        id: Int,
        fragment: Fragment
    ) {
        val fragmentTransaction: FragmentTransaction = manager.beginTransaction()
        fragmentTransaction.replace(id, fragment)
        fragmentTransaction.addToBackStack(fragment.javaClass.getSimpleName())
        fragmentTransaction.commit()
    }

    fun formatedValues(value: Int): String? {
        return java.lang.String.format(Locale.ENGLISH, "%02d", value)
    }

    fun formatedValues(value: Double?): String? {
        return java.lang.String.format(Locale.ENGLISH, "%.2f", value)
    }

    fun formatedValues(value: String): String? {
        return if (!value.equals("", ignoreCase = true)) java.lang.String.format(
            Locale.ENGLISH,
            "%.2f",
            value.toDouble()
        ) else "0.00"
    }


    fun log(TAG: String?, content: String?) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, content!!)
        }
    }

    fun loadImageBanner(imageview: ImageView, imageUrl: String?) {
        if (imageUrl == null) {
            return
        }
        Glide.with(imageview.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
            )
            .into(imageview)
    }

    fun convertDate(date: Date, format: String?): String? {
        val simpleDateFormat = SimpleDateFormat(format, Locale.ENGLISH)
        return simpleDateFormat.format(date)
    }

    fun convertDate(date: String, inputFormat: String?, outputFormat: String?): String? {
        val simpleDateFormat = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        var date1: Date? = null
        try {
            date1 = simpleDateFormat.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val simpleOutputFormat = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return if (date1 != null) {
            simpleOutputFormat.format(date1)
        } else null
    }

    fun convertDateutc(date: String, inputFormat: String?, outputFormat: String?): String? {
        val simpleDateFormat = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        var date1: Date? = null
        try {
            date1 = simpleDateFormat.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val simpleOutputFormat = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return if (date1 != null) {
            simpleOutputFormat.format(date1)
        } else null
    }

    fun getConvertedTime(createdTime: String): String {
        val simpleDateFormat:SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH)
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        var targetdatevalue = ""
        try {
            val startDate: Date? = simpleDateFormat.parse(createdTime)
            val targetFormat: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
            targetFormat.setTimeZone(Calendar.getInstance().getTimeZone())
            targetdatevalue = targetFormat.format(startDate!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return targetdatevalue
    }

    fun getFormatedString(createdTime: String, format: String?): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH)
        var targetdatevalue = ""
        try {
            val startDate: Date? = simpleDateFormat.parse(createdTime)
            val targetFormat = SimpleDateFormat(format, Locale.ENGLISH)
            targetdatevalue = targetFormat.format(startDate!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return targetdatevalue
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.getResources().getDisplayMetrics().densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

      //  return dp * (context.getResources().getDisplayMetrics().densityDpi as Float / DisplayMetrics.DENSITY_DEFAULT)
    }

}

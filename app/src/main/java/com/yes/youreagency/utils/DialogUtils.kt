package com.yes.youreagency.utils

import android.app.*
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import kotlin.math.roundToInt

import com.yes.youreagency.`interface`.DialogCallBack
import com.yes.youreagency.R
import com.yes.youreagency.session.Constants
import com.yes.youreagency.session.SharedHelper


object DialogUtils {

    var loaderDialog: Dialog? = null
    var noInternetDialog: Dialog? = null
    private var isShowingDialog = false

    fun getBitmapfromUrl(imageUrl: String?): Bitmap? {
        return try {
            //val urlImage = "https://thumbs.dreamstime.com/z/hands-holding-blue-earth-cloud-sky" + "-elements-imag-background-image-furnished-nasa-61052787.jpg"
            val url = URL(imageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            Log.e("awesome", "Error in getting notification image: " + e.localizedMessage)
            null
        }
    }

    fun showPictureDialog(activity: Activity) {

        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle("Choose Your Option")
        val items = arrayOf("Gallery", "Camera")
        //val items = arrayOf("Gallery")

        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.READ_STORAGE_PERM_LIST,
                        Constants.Permission.READ_STORAGE_PERMISSIONS
                    )
                    BaseUtils.openGallery(activity)
                } else {
                    BaseUtils.openGallery(activity)
                }
                1 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.CAMERA_PERM_LIST,
                        Constants.Permission.CAMERA_STORAGE_PERMISSIONS
                    )
                    BaseUtils.openCamera(activity)
                } else {
                    BaseUtils.openCamera(activity)
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }


    fun showLoader(context: Context) {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

        if(SharedHelper(context).loader_common.isEmpty() || SharedHelper(context).loader_common.equals("null")){
            Glide.with(context).load(R.drawable.loader_common).into(loaderDialog!!.findViewById(R.id.image))
        }
        else{
            Glide.with(context).load(SharedHelper(context).loader_common).into(loaderDialog!!.findViewById(R.id.image))
        }

        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }

    }

    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }

    fun showAlertDialog(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = dialog.findViewById<TextView>(R.id.header)
        var contentTextView = dialog.findViewById<TextView>(R.id.content)

        var cancel = dialog.findViewById<Button>(R.id.cancel)
        var ok = dialog.findViewById<Button>(R.id.ok)

        if(title.equals("")){
            headerTextView.visibility = View.GONE
        }

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        UiUtils.button(cancel)
        UiUtils.button(ok)

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick("")
            dialog.dismiss()
        }

        dialog.show()

    }


}
package com.yes.youreagency.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.yes.youreagency.network.UrlHelper
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.utils.BaseUtils
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.viewmodels.MapViewModel
import com.yes.youreagency.databinding.ActivityLoginBinding
import com.yes.youreagency.session.Constants

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding
    var mapViewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var requestCode = 0
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            //val data: Intent? = result.data
            if(requestCode == 51){
                if(!BaseUtils.isGpsEnabled(this)){
                    BaseUtils.gpsEnableRequest(this)
                }
                else {
                    proceed()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_login)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
        sharedHelper!!.branchid = UrlHelper.MASTERID.toInt()
        UiUtils.textview(binding.back)
        UiUtils.textview(binding.signup)
        UiUtils.button(binding.login)
        UiUtils.notificationbar(this)

        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.login.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                mapViewModel?.login(this,binding.mobile.text.toString(),binding.password.text.toString())?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            }
                            else {
                                if(it.login_data!!.user_type.equals("4")){
                                    sharedHelper!!.token = it.token.toString()
                                    sharedHelper!!.loggedIn = true
                                    sharedHelper?.id = it.login_data!!.id!!.toInt()
                                    sharedHelper?.name = it.login_data!!.name!!
                                    if(it.login_data!!.email == null || it.login_data!!.email!!.isEmpty() || it.login_data!!.email.equals("") || it.login_data!!.email.equals("null")){
                                        sharedHelper?.email = ""
                                    }
                                    else{
                                        sharedHelper!!.email = it.login_data!!.email!!
                                    }

                                    sharedHelper?.mobileNumber = it.login_data!!.mobile!!
                                    sharedHelper?.countryCode = it.login_data!!.country_code!!
                                    if(it.referral!!.size > 0){
                                        Log.d("dgfhdj1",""+ it.referral!![0][0].referral_code)
                                        sharedHelper!!.refercode = it.referral!![0][0].referral_code.toString()
                                    }
                                    startActivity(Intent(this, DashBoardActivity::class.java))
                                    finish()
                                }
                                else{
                                    UiUtils.showSnack(binding.root, "Not Valid")
                                }
                            }
                        }
                    }
                })
            }
        }


        if(!BaseUtils.isPermissionsEnabled(this@LoginActivity) || !BaseUtils.isGpsEnabled(this@LoginActivity)){
            BaseUtils.permissionsEnableRequest(this@LoginActivity)
        }
        else{
            proceed()
        }

    }

    private fun isValidInputs(): Boolean {
        when {
            binding.mobile.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,"Please Enter Mobile Number")
                return false
            }
            binding.mobile.length() != 10 -> {
                UiUtils.showSnack(binding.root,"Please Enter Valid Mobile Number")
                return false
            }
            binding.password.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,"Please Enter Password")
                return false
            }
        }
        return true
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("xcbnc",""+requestCode)
        val code: Int = 200
        when (requestCode) {
            code -> if (grantResults.size > 0) {
                val locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED
                if (locationAccepted && storageAccepted && cameraAccepted) {
                    //all permissions granted
                    if(!BaseUtils.isGpsEnabled(this@LoginActivity)){
                        BaseUtils.gpsEnableRequest(this@LoginActivity)
                    }
                    else{
                        proceed()
                    }
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(
                                Manifest.permission.CAMERA
                            ) || shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        ) {
                            BaseUtils.permissionsEnableRequest(this)
                            return
                        } else {
                            BaseUtils.displayManuallyEnablePermissionsDialog(this@LoginActivity)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       // super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 51){
            if(!BaseUtils.isGpsEnabled(this)){
                BaseUtils.gpsEnableRequest(this)
            }
            else {
                proceed()
            }
        }
    }

    fun proceed(){
        if(BaseUtils.isPermissionsEnabled(this) && BaseUtils.isGpsEnabled(this)) {
            sharedHelper?.location = ""
            if(sharedHelper!!.loggedIn) {
                startActivity(Intent(this, DashBoardActivity::class.java))
                finish()
            }
        }
    }
}
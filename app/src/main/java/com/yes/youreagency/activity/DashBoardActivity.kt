package com.yes.youreagency.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

import androidx.lifecycle.ViewModelProvider

import android.widget.Toast
import com.yes.youreagency.`interface`.DialogCallBack
import com.yes.youreagency.fragment.OrdersFragment

import com.yes.youreagency.fragment.QuickOrderFragment
import com.yes.youreagency.R
import com.yes.youreagency.responses.Data
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.viewmodels.MapViewModel
import com.yes.youreagency.databinding.ActivityDashboardBinding

class DashBoardActivity : BaseActivity()  {
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var binding: ActivityDashboardBinding
    lateinit var sharedHelper: SharedHelper
    var mapViewModel: MapViewModel? = null
    var data1: Data? = null
    var notificationsBadge : View?  = null
    var isSpinnerTouched = false
    var doubleBackToExitPressedOnce = false
    var dX = 0f
    var dY = 0f
    var lastAction = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_dashboard)
        sharedHelper = SharedHelper(this)

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        binding.bottomNavigationMain1.setOnItemSelectedListener {it ->
            when (it.itemId) {
                R.id.quick -> {
                    if(!it.isCheckable) {
                        setAdapter(0)
                    }
                    true
                }
                R.id.order -> {
                    if(!it.isCheckable) {
                        setAdapter(1)
                    }
                    true
                }
                else -> false
            }
        }
        setAdapter(0)
    }


    override fun onResume() {
        super.onResume()
    }

    fun logout(){
        DialogUtils.showAlertDialog(this,"Are you sure you want to logout?","","Yes","Cancel", object :
            DialogCallBack {
            override fun onPositiveClick(value: String) {
                sharedHelper.loggedIn = false
                sharedHelper.token = ""
                sharedHelper.id = 0
                sharedHelper.name = ""
                sharedHelper.email = ""
                sharedHelper.mobileNumber = ""
                sharedHelper.countryCode = ""
                sharedHelper.refercode = ""

                startActivity(Intent(this@DashBoardActivity, LoginActivity::class.java))
                finish()
            }

            override fun onNegativeClick() {

            }
        })
    }

    fun setAdapter(position: Int) {
        when (position) {
            0 -> {
                navigationaddfragment(QuickOrderFragment(),0)
            }
            1 -> {
                navigationaddfragment(OrdersFragment(),1)
            }
        }
    }

    fun removefragment(fragment: Fragment){
        for (i in 0 until binding.bottomNavigationMain1.getMenu().size()) {
            if(i != 0){
                binding.bottomNavigationMain1.getMenu().getItem(i).setCheckable(false)
            }
        }
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()
        onBackPressed()
    }

    fun navigationaddfragment(fragment: Fragment,bnavid: Int){
        for (i in 0 until binding.bottomNavigationMain1.getMenu().size()) {
            binding.bottomNavigationMain1.getMenu().getItem(i).setCheckable(false)
        }

        binding.bottomNavigationMain1.getMenu().getItem(bnavid).setCheckable(true);
        binding.bottomNavigationMain1.getMenu().getItem(bnavid).setChecked(true);

        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.add(R.id.container, fragment,fragment.javaClass.name)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun addfragment(fragment: Fragment, bundle: Bundle?){
        for (i in 0 until binding.bottomNavigationMain1.getMenu().size()) {
            binding.bottomNavigationMain1.getMenu().getItem(i).setCheckable(false)
        }

        fragment.setArguments(bundle)
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.add(R.id.container, fragment,fragment.javaClass.name)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }


    override fun onBackPressed() {
        for (i in 0 until binding.bottomNavigationMain1.getMenu().size()) {
            if(i != 0){
                binding.bottomNavigationMain1.getMenu().getItem(i).setCheckable(false)
            }
        }

        if(supportFragmentManager.backStackEntryCount == 1){
            if (doubleBackToExitPressedOnce) {
                finish()
                return
            }
            doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
        else{
            super.onBackPressed()
        }
    }

}
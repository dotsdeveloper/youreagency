package com.yes.youreagency.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.viewmodels.MapViewModel
import com.yes.youreagency.databinding.ActivitySuccessBinding


class SuccessActivity : AppCompatActivity() {
    lateinit var binding: ActivitySuccessBinding
    var mapViewModel: MapViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_success)
        binding = ActivitySuccessBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        UiUtils.textview(binding.msgSuccess)
        UiUtils.textview(binding.trackorder)
        UiUtils.textviewbg(binding.shopmore)
        UiUtils.imageviewtint(binding.img)
        UiUtils.notificationbar(this)

        binding.orderid.text = "Your order number is # "+intent.getStringExtra("orderid").toString()

        binding.shopmore.setOnClickListener {
            finish()
            val intent = Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("isorder",true)
            startActivity(intent)
        }

        binding.trackorder.setOnClickListener {
            if(intent.getStringExtra("orderid").toString().isNotEmpty()){

            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("isorder",true)
        startActivity(intent)
    }
}
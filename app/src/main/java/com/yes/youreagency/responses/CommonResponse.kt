package com.yes.youreagency.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommonResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = "Invalid Response"

}
package com.yes.youreagency.responses

import com.google.gson.annotations.SerializedName

class GetCustomerResponse {
    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<LoginData>? = null
}
package com.yes.youreagency.adapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.yes.youreagency.fragment.OrdersFragment
import com.yes.youreagency.R
import com.yes.youreagency.responses.OrderData
import com.yes.youreagency.session.Constants
import com.yes.youreagency.utils.BaseUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.databinding.CardOrderlistBinding
import com.yes.youreagency.fragment.OrderDetailFragment
import com.yes.youreagency.fragment.QuickOrderFragment
import com.yes.youreagency.utils.DialogUtils

class OrderListAdapter(
    var ordersFragment: OrdersFragment,
    var context: Context,
    var list: ArrayList<OrderData>?
) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrderlistBinding = CardOrderlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_orderlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //UiUtils.textviewbg(holder.binding.reorder)
        UiUtils.textview(holder.binding.reorder)
        //UiUtils.textviewbg(holder.binding.details)
        UiUtils.textview(holder.binding.details)
        holder.binding.orderid.text = list!![position].invoice.toString()
        holder.binding.orderamount.text = "Amount : "+ Constants.Common.CURRENCY+" "+list!![position].grand_total.toString()

        if(list!![position].items!!.size == 1){
            holder.binding.items.text = list!![position].items!!.size.toString()+" Item"
        }
        else{
            holder.binding.items.text = list!![position].items!!.size.toString()+" Items"
        }

        holder.binding.date.text = BaseUtils.getFormatedDate(list!![position].created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","dd")
        holder.binding.year.text = BaseUtils.getFormatedDate(list!![position].created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","yyyy")
        holder.binding.month.text = BaseUtils.getFormatedDate(list!![position].created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","MMMM")
        holder.binding.time.text = BaseUtils.getFormatedDate(list!![position].created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'","hh:mm aaa")

        holder.binding.root.setOnClickListener {
            val args = Bundle()
            args.putSerializable("key", list!![position])
            ordersFragment.dashBoardActivity!!.addfragment(OrderDetailFragment(),args)
        }
        holder.binding.details.setOnClickListener {
            val args = Bundle()
            args.putSerializable("key", list!![position])
            ordersFragment.dashBoardActivity!!.addfragment(OrderDetailFragment(),args)
        }

        holder.binding.reorder.setOnClickListener {
            DialogUtils.showLoader(context)
            ordersFragment.mapViewModel?.reorder(context,list!![position].id!!)?.observe(ordersFragment, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            UiUtils.showSnack(holder.binding.root, it.message!!)
                        }
                        else {
                            ordersFragment.dashBoardActivity!!.addfragment(QuickOrderFragment(),null)
                        }
                    }
                }
            })
        }
    }
}
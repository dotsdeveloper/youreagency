package com.yes.youreagency.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.github.razir.progressbutton.DrawableButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.button.MaterialButton
import com.yes.Responses.Responses.Order
import com.yes.youreagency.fragment.QuickOrderFragment
import com.yes.youreagency.R
import com.yes.youreagency.session.Constants
import com.yes.youreagency.session.SharedHelper
import com.yes.youreagency.utils.DialogUtils
import com.yes.youreagency.utils.UiUtils
import com.yes.youreagency.databinding.CardCartlistBinding
import kotlin.collections.ArrayList

class CartlistAdapter(
    var quickOrderFragment: QuickOrderFragment = QuickOrderFragment(),
    var key:Int,
    var context: Context,
    var list: ArrayList<Order>?
) :
    RecyclerView.Adapter<CartlistAdapter.HomeHeaderViewHolder>() {
    var sharedHelper: SharedHelper? = null
        inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCartlistBinding = CardCartlistBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        sharedHelper = SharedHelper(context)
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_cartlist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
         if(key == 1){
            UiUtils.plus_minus(holder.binding.plus)
            UiUtils.plus_minus(holder.binding.minus)
            UiUtils.textview(holder.binding.qty)
            // UiUtils.offerborder(holder.binding.offer)
            if(list!![position].product!!.brand_id != 0){
                holder.binding.text.text = list!![position].product!!.brand.name+" - "+list!![position].product!!.name.toString()
            }
            else{
                holder.binding.text.text = list!![position].product!!.name.toString()
            }

            if(list!![position].product!!.files!!.isNotEmpty()){
                UiUtils.loadImage(holder.binding.imageViewbanner,list!![position].product!!.files!![0])
            }

            holder.binding.qty.text = list!![position].quantity.toString()
            holder.binding.details2.text = list!![position].quantity.toString()+"X "+ Constants.Common.CURRENCY+UiUtils.formatedValues(list!![position].amount.toString())
            holder.binding.details1.text = "("+list!![position].quantities!!.quantity.toString()+" "+list!![position].quantities!!.product_unit!!.name.toString()+")"
            holder.binding.details3.text = Constants.Common.CURRENCY+UiUtils.formatedValues(list!![position].total.toString())

            if(list!![position].save_value!!.toDouble() != 0.0){
                holder.binding.offer.visibility = View.VISIBLE
                UiUtils.offerborder(holder.binding.offer)
                holder.binding.offer.text = Constants.Common.CURRENCY+list!![position].save_value.toString()+" SAVED"
            }
            else{
                holder.binding.offer.visibility = View.GONE
            }

            if(position != 0){
                holder.binding.category.text = list!![position].product!!.category!!.name.toString()
                if(list!![position].product!!.category!!.ccount == 1){
                    holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Item"
                }
                else{
                    holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Items"
                }
                var old = list!![position-1].product!!.category!!.name.toString()
                var new = list!![position].product!!.category!!.name.toString()
                if(old.equals(new)){
                    holder.binding.linearCat.visibility = View.GONE
                }
                else{
                    holder.binding.linearCat.visibility = View.VISIBLE
                }
            }
            else{
                holder.binding.category.text = list!![position].product!!.category!!.name.toString()
                if(list!![position].product!!.category!!.ccount == 1){
                    holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Item"
                }
                else{
                    holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Items"
                }
                holder.binding.linearCat.visibility = View.VISIBLE
            }

            holder.binding.plus.setOnClickListener {
                if(sharedHelper!!.stock_option == 1){
                    if(holder.binding.qty.text.toString().toInt() > sharedHelper!!.max_quantity && sharedHelper!!.max_quantity != 0) {
                        UiUtils.showSnack(holder.binding.root,"Sorry you can not add more quantity.")
                    }
                    else{
                        if((sharedHelper!!.max_quantity == 0 || sharedHelper!!.max_quantity > holder.binding.qty.text.toString().toInt()) && holder.binding.qty.text.toString().toInt() < list!![position].quantities!!.stock.toString().toInt()){
                            //DialogUtils.showCartLoader(context)
                            plusclickstart(holder.binding.plus)
                            quickOrderFragment.mapViewModel?.addtocart(context,holder.binding.qty.text.toString().toInt()+1,list!![position].quantities!!.id,list!![position].product_id!!.toInt())
                                ?.observe(quickOrderFragment, Observer {
                                    //DialogUtils.dismissLoader()
                                    it?.let {
                                        it.error?.let { error ->
                                            if (error) {
                                                plusclickend(holder.binding.plus)
                                                UiUtils.showSnack(holder.binding.root, it.message!!)
                                            } else {
                                                plusclickend(holder.binding.plus)
                                                if(!sharedHelper!!.loggedIn){
                                                    sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                                }
                                                quickOrderFragment.getcart(false,false,true,false)
                                                list!![position].quantity = (holder.binding.qty.text.toString().toInt()+1).toString()
                                                list!![position].total = UiUtils.formatedValues(((holder.binding.qty.text.toString().toInt()+1)*(list!![position].amount!!.toDouble())).toString())
                                                notifyItemChanged(position)
                                            }
                                        }
                                    }
                                })
                        }
                        else{
                            UiUtils.showSnack(holder.binding.root,"Out of Stock")
                        }
                    }
                }
                else if(sharedHelper!!.stock_option == 0){
                    if(holder.binding.qty.text.toString().toInt() > sharedHelper!!.max_quantity && sharedHelper!!.max_quantity != 0){
                        UiUtils.showSnack(holder.binding.root,"Sorry you can not add more quantity.")
                    }
                    else{
                        // DialogUtils.showCartLoader(context)
                        plusclickstart(holder.binding.plus)
                        quickOrderFragment.mapViewModel?.addtocart(context,holder.binding.qty.text.toString().toInt()+1,list!![position].quantities!!.id,list!![position].product_id!!.toInt())
                            ?.observe(quickOrderFragment, Observer {
                                //DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            plusclickend(holder.binding.plus)
                                            UiUtils.showSnack(holder.binding.root, it.message!!)
                                        } else {
                                            plusclickend(holder.binding.plus)
                                            if(!sharedHelper!!.loggedIn){
                                                sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                            }
                                            quickOrderFragment.getcart(false,false,true,false)
                                            list!![position].quantity = (holder.binding.qty.text.toString().toInt()+1).toString()
                                            list!![position].total = UiUtils.formatedValues(((holder.binding.qty.text.toString().toInt()+1)*(list!![position].amount!!.toDouble())).toString())
                                            notifyItemChanged(position)
                                        }
                                    }
                                }
                            })
                    }
                }
            }
            holder.binding.minus.setOnClickListener {
                if(holder.binding.qty.text.toString().toInt() > 0 && holder.binding.qty.text.toString().toInt() != 1){
                    //DialogUtils.showCartLoader(context)
                    minusclickstart(holder.binding.minus)
                    quickOrderFragment.mapViewModel?.addtocart(context,holder.binding.qty.text.toString().toInt()-1,list!![position].quantities!!.id,list!![position].product_id!!.toInt())
                        ?.observe(quickOrderFragment, Observer {
                            //DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        minusclickend(holder.binding.minus)
                                        UiUtils.showSnack(holder.binding.root, it.message!!)
                                    } else {
                                        minusclickend(holder.binding.minus)
                                        if(!sharedHelper!!.loggedIn){
                                            sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                        }
                                        quickOrderFragment.getcart(false,false,true,false)
                                        list!![position].quantity = (holder.binding.qty.text.toString().toInt()-1).toString()
                                        list!![position].total = UiUtils.formatedValues(((holder.binding.qty.text.toString().toInt()-1)*(list!![position].amount!!.toDouble())).toString())
                                        notifyItemChanged(position)
                                    }
                                }
                            }
                        })
                }
                else if(holder.binding.qty.text.toString().toInt() == 1){
                    //DialogUtils.showCartLoader(context)
                    minusclickstart(holder.binding.minus)
                    quickOrderFragment.mapViewModel?.addtocart(context,holder.binding.qty.text.toString().toInt()-1,list!![position].quantities!!.id,list!![position].product_id!!.toInt())
                        ?.observe(quickOrderFragment, Observer {
                            //DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        minusclickend(holder.binding.minus)
                                        UiUtils.showSnack(holder.binding.root, it.message!!)
                                    } else {
                                        minusclickend(holder.binding.minus)
                                        if(!sharedHelper!!.loggedIn && (holder.binding.qty.text.toString().toInt()-1)!=0){
                                            sharedHelper!!.cartid_temp = it.addcart_data!!.cart_id.toString()
                                        }
                                        var scatname = list!![position].product!!.category!!.name.toString()
                                        var scatcount = list!![position].product!!.category!!.ccount!!.toInt()
                                        for(items in list!!){
                                            if(items.product!!.category!!.name.equals(scatname)){
                                                items.product!!.category!!.ccount = scatcount-1
                                            }
                                        }
                                        list!!.removeAt(position)
                                        notifyDataSetChanged()
                                        quickOrderFragment.getcart(false,false,false,true)
                                    }
                                }
                            }
                        })
                }
            }
            holder.binding.delete.setOnClickListener {
                DialogUtils.showLoader(context)
                quickOrderFragment.mapViewModel?.deletecart(context,list!![position].id!!.toInt())
                    ?.observe(quickOrderFragment, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    UiUtils.showSnack(holder.binding.root, it.message!!)
                                } else {
                                    var scatname = list!![position].product!!.category!!.name.toString()
                                    var scatcount = list!![position].product!!.category!!.ccount!!.toInt()
                                    for(items in list!!){
                                        if(items.product!!.category!!.name.equals(scatname)){
                                            items.product!!.category!!.ccount = scatcount-1
                                        }
                                    }
                                    list!!.removeAt(position)
                                    notifyDataSetChanged()
                                    quickOrderFragment.getcart(false,false,false,true)
                                }
                            }
                        }
                    })
            }

        }
    }

    fun plusclickstart(button: MaterialButton){
        button.icon = null
        button.showProgress {
            progressColor = Color.BLACK
            gravity = DrawableButton.GRAVITY_CENTER
        }
        button.isEnabled = false
    }

    fun plusclickend(button: MaterialButton){
        button.isEnabled = true
        button.icon = context.getDrawable(R.drawable.ic_baseline_add_24)
        button.hideProgress("")
    }

    fun minusclickstart(button: MaterialButton){
        button.icon = null
        button.showProgress {
            progressColor = Color.BLACK
            gravity = DrawableButton.GRAVITY_CENTER
        }
        button.isEnabled = false
    }

    fun minusclickend(button: MaterialButton){
        button.isEnabled = true
        button.icon = context.getDrawable(R.drawable.ic_baseline_remove_24)
        button.hideProgress("")
    }
}
package com.yes.youreagency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.youreagency.activity.CustomerActivity
import com.yes.youreagency.R
import com.yes.youreagency.responses.LoginData
import com.yes.youreagency.databinding.CardCustomerBinding

class CustomerListAdapter(
    var customerActivity: CustomerActivity?,
    var context: Context,
    var list: ArrayList<LoginData>?
) :
    RecyclerView.Adapter<CustomerListAdapter.ViewHolder>() {
    var selectedPosition = -1

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCustomerBinding = CardCustomerBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_customer,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.name.text = list!![position].name
        holder.binding.mobile.text = list!![position].mobile
        if(list!![position].email != null && !list!![position].email.equals("")){
            holder.binding.email.text = list!![position].email
            holder.binding.email.visibility = View.VISIBLE
        }
        else{
            holder.binding.email.visibility = View.GONE
        }

        if(selectedPosition == position){
            holder.binding.radio.isChecked = true
        }
        else {
            holder.binding.radio.isChecked = false
            holder.binding.linear.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                customerActivity!!.binding.proceed3.visibility = View.VISIBLE
                customerActivity!!.isselect = true
                customerActivity!!.suserid = list!![position].id!!.toInt()
                customerActivity!!.smobile = list!![position].mobile.toString()
                customerActivity!!.sname = list!![position].name.toString()
            }
        }
    }
}
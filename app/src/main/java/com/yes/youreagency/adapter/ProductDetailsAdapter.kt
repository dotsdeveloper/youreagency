package com.yes.youreagency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.Responses.Responses.Order
import com.yes.youreagency.R
import com.yes.youreagency.databinding.CardProductdetailsBinding
import com.yes.youreagency.fragment.OrderDetailFragment
import com.yes.youreagency.session.Constants
import com.yes.youreagency.utils.UiUtils


class ProductDetailsAdapter(
    var orderDetailFragment: OrderDetailFragment,
    var context: Context,
    var list: ArrayList<Order>?
) :
    RecyclerView.Adapter<ProductDetailsAdapter.HomeHeaderViewHolder>() {
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardProductdetailsBinding = CardProductdetailsBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_productdetails,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        if(list!![position].product!!.brand_id != 0){
            holder.binding.text.text = list!![position].product!!.brand.name+" - "+list!![position].product!!.name.toString()
        }
        else{
            holder.binding.text.text = list!![position].product!!.name.toString()
        }

        if(list!![position].product!!.files!!.isNotEmpty()){
            UiUtils.loadImage(holder.binding.imageViewbanner,list!![position].product!!.files!![0])
        }

        holder.binding.qty.text = list!![position].quantity.toString()+" Quantity"
        holder.binding.details.text = list!![position].quantities!!.quantity.toString()+" "+list!![position].quantities!!.product_unit!!.name

        holder.binding.price.text = Constants.Common.CURRENCY+list!![position].quantities!!.amount

        holder.binding.category.text = list!![position].product!!.category!!.name.toString()

        if(list!![position].save_value!!.toDouble() != 0.0){
            holder.binding.offer.visibility = View.VISIBLE
            holder.binding.offer.text = Constants.Common.CURRENCY+list!![position].save_value.toString()+" SAVED"
        }
        else{
            holder.binding.offer.visibility = View.GONE
        }

        if(position != 0){
            holder.binding.category.text = list!![position].product!!.category!!.name.toString()
            if(list!![position].product!!.category!!.ccount == 1){
                holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Item"
            }
            else{
                holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Items"
            }
            var old = list!![position-1].product!!.category!!.name.toString()
            var new = list!![position].product!!.category!!.name.toString()
            if(old.equals(new)){
                holder.binding.linearCat.visibility = View.GONE
            }
            else{
                holder.binding.linearCat.visibility = View.VISIBLE
            }
        }
        else{
            holder.binding.category.text = list!![position].product!!.category!!.name.toString()
            if(list!![position].product!!.category!!.ccount == 1){
                holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Item"
            }
            else{
                holder.binding.count.text = list!![position].product!!.category!!.ccount.toString()+" Items"
            }
        }

    }
}